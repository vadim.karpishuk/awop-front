import logo from './logo.svg';
import appStyles from './App.module.css';
import React from "react";
import News from './components/news/News';
import {Switch, Route, NavLink} from "react-router-dom";
import MainBody from "./components/mainbody/MainBody";
import axios from "axios";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount() {
        axios.get("/api/v1/awop-back/user/currentUser").then(response => {
            if (response.status === 200) {
                this.setState({currentUser: response.data});
            }
        }).catch(error => {
            if (error.response.status === 404) {
                this.setState({currentUser: undefined});
            }
        });
    }

    handleLogout(event) {
        event.preventDefault();
        axios.get("/logout").then(response => {
            if (response.status === 302) {
                this.setState({currentUser: undefined});
            }
        }).catch(error => {
            if (error.response.status === 404) {
                this.setState({currentUser: undefined});
            }
        });
    }

    render(props) {
        return (
            <div className={appStyles.App}>
                <header className={appStyles.AppHeader}>
                    {/*<div className={appStyles.blockLoginRegister}>*/}
                    {/*    */}
                    {/*</div>*/}
                    <div className={appStyles.banner}>
                        <div className={appStyles.bannerMainProxy}>
                            <NavLink to="/" className={appStyles.bannerMain}>
                                A World Of Pain
                            </NavLink>
                        </div>
                        <div className={appStyles.buttonContainer}>
                            <NavLink to="/news" className={appStyles.buttonCustom}>News</NavLink>
                            <NavLink to="/plugins" className={appStyles.buttonCustom}>Plugins</NavLink>
                            <NavLink to="/forum" className={appStyles.buttonCustom}>Forum</NavLink>
                            {this.state.currentUser === undefined && <NavLink to="/login" className={appStyles.buttonCustom + ' ' + appStyles.buttonLR + ' ' + appStyles.login}>Login</NavLink>}
                            {this.state.currentUser === undefined && <NavLink to="/register" className={appStyles.buttonCustom + ' ' + appStyles.buttonLR + ' ' + appStyles.register}>Register</NavLink>}
                            {this.state.currentUser !== undefined &&
                                <div className={appStyles.dropdownUser}>
                                    <span>{this.state.currentUser.name}</span>
                                    <div className={appStyles.dropdownContentUser}>
                                        <NavLink to={`/profile/${this.state.currentUser.name}`}>Profile</NavLink>
                                        <p>Forum threads</p>
                                        <p>Premium</p>
                                        <button onClick={this.handleLogout}>Logout</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                    <div>
                        <MainBody/>
                    </div>
                </header>
            </div>
        )
    }
}

export default App;
