import React from "react";
import axios from "axios";

class UserProfile extends React.Component {

    state = {}

    componentDidMount() {
        this.setState({userToDisplayName: this.props.match.params.username});
        axios.get("/api/v1/awop-back/user/currentUser").then(response => {
            if (response.status === 200) {
                this.setState({currentUser: response.data, auth: true});
            }
        }).catch(error => {
            if (error.response.status === 404) {
                this.setState({auth: false});
            }
        });
    }

    render() {
        return (<div>
            { this.state.auth === true && this.state.currentUser.name === this.state.userToDisplayName &&
                    <div>
                        <div>{this.state.currentUser.name}</div>
                        <div>{this.state.currentUser.email}</div>
                        <div>{this.state.currentUser.createdAt}</div>
                    </div>
            }
        </div>);
    }
}

export default UserProfile;