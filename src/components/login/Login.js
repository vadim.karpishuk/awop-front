import React, { Component } from "react";
import axios from "axios";
import styles from "./login.module.css";
import appStyles from "../../App.module.css";
import {NavLink} from "react-router-dom";


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {name: "", password: "", notCorrectCreds: false};

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePasswdChange = this.handlePasswdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event){
        event.preventDefault();
        this.setState({name: event.target.value});
    }

    handlePasswdChange(event){
        event.preventDefault();
        this.setState({password: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        alert('handling login');
        let token = window.btoa(this.state.name + ":" + this.state.password);
        axios.get("/user", {
            headers : {
                'Authorization': `Basic ${token}`
            }
        }).then(response => {
            this.setState({notCorrectCreds: false})
            }
        ).catch((error) => {
           if (error.response.status === 401) {
               // alert('user auth unsuccessfull!!!');
               this.setState({notCorrectCreds: true})
           }
        });
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.wrapForm}>
                    <form className={styles.loginForm} onSubmit={this.handleSubmit}>
                        <span className={styles.formTitle}>Login</span>
                        <span className={styles.labelInput}>Username</span>
                        <div className={styles.iconAndInputContainerUsername}>
                            <input type="text" name="username" placeholder= "Type your username" value = {this.state.name} onChange={this.handleNameChange}/>
                        </div>
                        <span className={styles.labelInput}>Password</span>
                        <div className={styles.iconAndInputContainerPassword}>
                            <input type="password" name="password" placeholder="Type your password" value = {this.state.password} onChange={this.handlePasswdChange}/>
                        </div>
                        {this.state.notCorrectCreds === true && <p>Not correct creds!</p>}
                        <div className={styles.textRight}>
                            <a href="#">Forgot password?</a>
                        </div>
                        <div>
                            <button className={styles.button}>LOGIN</button>
                        </div>
                        <NavLink to="/register" className={styles.signUpLink}>Sign Up</NavLink>
                        <div className={styles.socialEntireContainer}>
                            <a href="#" className={styles.socialEntire}>
                                <i className="fa-brands fa-facebook-f fa-xl"></i>
                            </a>
                            <a href="#" className={styles.socialEntire}>
                                <i className="fa-brands fa-twitter fa-xl"></i>
                            </a>
                            <a href="#" className={styles.socialEntire}>
                                <i className="fa-brands fa-google fa-xl"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;