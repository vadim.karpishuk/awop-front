import React from "react";
import {Route, Switch} from "react-router-dom";
import News from "../news/News";
import Plugins from "../plugins/Plugins"
import Register from "../register/Register";
import Login from "../login/Login";
import UserProfile from "../user/UserProfile"
import mainBodyStyles from './mainbody.module.css';
import axios from "axios";

class MainBody extends React.Component {

    state = {}

    // componentDidMount = () => {
    //     axios.get("/api/get/name").then(response => {
    //         this.setState({
    //             name: response.data,
    //         })
    //     })
    //     axios.get("/api/get/lastModified").then(response => {
    //         this.setState({
    //             lastModified: response.data,
    //         })
    //     })
    //     axios.get("/api/get/links").then(response => {
    //         this.setState({
    //             links: response.data,
    //         })
    //     })
    // }

    componentDidMount() {
        axios.get("/api/v1/awop-back/user/currentUser").then(response => {
           if (response.status === 200) {
               this.setState({currentUser: response.data});
           }
        }).catch(error => {

        });
    }

    render() {
        return (
            <div className={mainBodyStyles.mainBodyContainer}>
                <div className={mainBodyStyles.leftContainer}>
                    <Switch>
                        <Route exact path="/" render={() => {
                            return (
                                <>Welcome! You are at main web resource, created for supporting my plugins!<br/>
                                    There you can checkout my plugins, discuss features, report about bugs and more!
                                </>
                            );
                        }}/>
                        <Route exact path="/news" component={News}/>
                        <Route exact path="/plugins" component={Plugins}/>
                        <Route exact path="/forum"/>
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/register" component={Register}/>
                        <Route path="/profile/:username" component={UserProfile} />
                    </Switch>
                </div>
            </div>
        );
    }

}

export default MainBody;