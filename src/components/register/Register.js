import React, { Component } from "react";
import axios from "axios";
import styles from "./register.module.css";

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = { name: "",
            role: "USER",
            email: "",
            password: "",
            duplicatedName: false,
            duplicatedEmail: false,
            registered: false
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswdChange = this.handlePasswdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event){
        event.preventDefault();
        this.setState({name: event.target.value});
    }

    handleEmailChange(event){
        event.preventDefault();
        this.setState({email: event.target.value});
    }

    handlePasswdChange(event){
        event.preventDefault();
        this.setState({password: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        axios.get(`/api/v1/awop-back/user/preRegister/check/name/${this.state.name}`)
            .then(response => {
                if (response.status === 204) {
                    this.setState({duplicatedName: false});
                }
                axios.get(`/api/v1/awop-back/user/preRegister/check/email/${this.state.email}`)
                    .then(response => {
                        if (response.status === 204) {
                            this.setState({duplicatedEmail: false});
                        }
                        if (!this.state.duplicatedName && !this.state.duplicatedEmail) {
                            axios.post("/api/v1/awop-back/user/register", {
                                name: this.state.name,
                                role: this.state.role,
                                email: this.state.email,
                                password: this.state.password
                            }).then(response => {
                                this.setState({registered: true});
                                setTimeout(() => {
                                    this.props.history.push('/news')
                                }, 5000);
                            })
                        }
                    }).catch(error => {
                    if (error.response.status === 400) {
                        this.setState({duplicatedEmail: true})
                    }
                });
            }).catch(error => {
                if (error.response.status === 400) {
                    this.setState({duplicatedName: true})
                }
            });
    }

    render() {
        return (
            /**********Делали с Вадимом******/
            /***Нужно вернуться к этой части позже**/
            /*<div>
                <h3>Controlled Component</h3>
                <br />
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Name:
                        <input type= "text" value={this.state.name} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Send" />
                </form>
            </div>*/

            /****Скопировала onSubmit, value, onChange и вставила по аналогии к Username*****/

            <div className={styles.container}>
                <div className={styles.wrapForm}>
                    <form className={styles.registerForm} onSubmit={this.handleSubmit}>
                        <span className={styles.formTitle}>Register</span>
                        {/*Email*/}
                        <span className={styles.labelInput}>Email</span>
                        <div className={styles.iconAndInputContainerEmail}>
                            <input type="text" name="email" placeholder= "Type your Email" value={this.state.email} onChange={this.handleEmailChange}/>
                        </div>
                        {/*Username*/}
                        <span className={styles.labelInput}>Username</span>
                        <div className={styles.iconAndInputContainerUsername}>
                            <input type="text" name="username" placeholder= "Type your username" value = {this.state.name} onChange={this.handleNameChange}/>
                        </div>
                        {/*Password*/}
                        <span className={styles.labelInput}>Password</span>
                        <div className={styles.iconAndInputContainerPassword}>
                            <input type="password" name="password" placeholder="Type your password" value = {this.state.password} onChange={this.handlePasswdChange}/>
                        </div>
                        {this.state.duplicatedName === true && <p>User with this nickname is already registered.</p>}
                        {this.state.duplicatedEmail=== true && <p>User with this email is already registered. Maybe you forgot the password?</p>}
                        {this.state.registered === true && <p>You are successfully registered. Verify your account via entered email!</p>}
                        {/*Button*/}
                        <div>
                            <button className={styles.button}>REGISTER</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Register;