const express = require("express");
const listenPort = "4000";
const server = express();
const news = require("./newsStub.json");
const name = require("./nameStub.json");
const lastModified = require("./lastModifiedStub.json");
const links = require("./linksStub.json")

server.get("/api/get/", (request, response) => {
    response.header("Content-Type", "application/json");
    response.send(JSON.stringify(news));
});

server.get("/api/get/lastModified", (request, response) => {
   response.header("Content-Type", "application/json");
   response.send(JSON.stringify(lastModified))
});

server.get("/api/get/links", (request, response) => {
    response.header("Content-Type", "application/json");
    response.send(JSON.stringify(links))
});

server.get("/api/get/name", (request, response) => {
    response.header("Content-Type", "application/json");
    response.send(JSON.stringify(name))
});

server.listen(listenPort, () => {
    console.log("Express server started!!!!");
});
