import React from "react";
import axios from "axios";
import styles from "./news.module.css";

class News extends React.Component {

    state = {
        isLoaded: false,
        news: [],
    }

    componentDidMount = () => {
        axios.get("/api/v1/awop-back/news").then(response => {
            console.log(response.data)
                this.setState({
                    news: response.data,
                    isLoaded: true
                })
            }
        )
    }


    render() {
        return (
            <div className={styles.newsContainerMultiple}>{this.state.news.map(item => {
                return (<div key={item.uuid} className={styles.newsContainer}>
                    <h2 className={styles.header}>
                        {/*<div className={styles.emptyFlexInHeader}/>*/}
                        <div className={styles.topicInHeader}>{item.topic}</div>
                        <div className={styles.dateInHeader}>{new Date(item.createdAt).toDateString()}</div>
                    </h2>
                    <hr></hr>
                    <p className={styles.newsContainerText}>{item.text}</p>
                </div>)
            })}</div>);
    }
}

export default News;