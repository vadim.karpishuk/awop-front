import React from "react";
import axios from "axios";
import styles from "./plugins.module.css";

class Plugins extends React.Component {
    state = {
        isLoaded: false,
        plugins: [],
        render: []
    }

    componentDidMount = () => {
        axios.get("/api/v1/awop-back/plugins/names").then(response => {
                this.setState({
                    plugins: response.data,
                    isLoaded: true
                })
            }
        ).catch(error => {
           if (error.response.status === 401) {
               
           }
        });
    }

    renderCertainPlugin (name) {
        let plugin
        axios.get("api/v1/awop-back/plugins/" + name).then(function (response) {
            plugin = response.data
        })
        console.log(plugin);
    }


    render() {
        return (
            <div className={styles.mainPluginsContainer}>{
                this.state.plugins.map(item => {
                    return <button className={styles.pluginContainer} onClick={() => this.renderCertainPlugin(item)}>{item}</button>
                })
            }
            </div>
        );
    }

}

export default Plugins;